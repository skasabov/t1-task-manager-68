package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "task"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskSaveRequest")
public class TaskSaveRequest {

    @NotNull
    protected TaskDto task;

    public TaskSaveRequest(@NotNull final TaskDto task) {
        this.task = task;
    }

}
