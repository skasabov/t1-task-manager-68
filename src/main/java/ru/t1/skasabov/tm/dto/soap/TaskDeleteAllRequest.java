package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "tasks"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskDeleteAllRequest")
public class TaskDeleteAllRequest {

    @NotNull
    protected List<TaskDto> tasks;

    public TaskDeleteAllRequest(@NotNull final List<TaskDto> tasks) {
        this.tasks = tasks;
    }

}
