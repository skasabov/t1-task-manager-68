package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "task"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskDeleteRequest")
public class TaskDeleteRequest {

    @NotNull
    @XmlElement(required = true)
    protected TaskDto task;

    public TaskDeleteRequest(@NotNull final TaskDto task) {
        this.task = task;
    }

}
