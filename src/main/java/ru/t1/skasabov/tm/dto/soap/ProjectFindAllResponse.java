package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "projects"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectFindAllResponse")
public class ProjectFindAllResponse {

    @XmlElement(name = "projects")
    protected List<ProjectDto> projects;

    public ProjectFindAllResponse(@NotNull final List<ProjectDto> projects) {
        this.projects = projects;
    }

}
