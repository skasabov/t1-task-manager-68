package ru.t1.skasabov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.skasabov.tm.config.ApplicationConfiguration;
import ru.t1.skasabov.tm.config.WebApplicationConfiguration;
import ru.t1.skasabov.tm.config.WebConfig;

import javax.servlet.ServletContext;

public final class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @NotNull
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { ApplicationConfiguration.class };
    }

    @NotNull
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebApplicationConfiguration.class, WebConfig.class };
    }

    @NotNull
    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

    @Override
    protected void registerContextLoaderListener(@NotNull final ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
    }

}
